package com.diseniowebdf.verifactura.scan;

import android.os.Bundle;

import com.diseniowebdf.verifactura.R;

public class SimpleScannerFragmentActivity extends BaseScannerActivity {
    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.activity_simple_scanner_fragment);
        setupToolbar();
    }
}