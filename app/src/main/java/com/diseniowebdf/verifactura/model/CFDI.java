package com.diseniowebdf.verifactura.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by davedega on 02/02/17.
 */
@DatabaseTable(tableName = "cfdi")
public class CFDI {

    @DatabaseField(columnName = "_id", generatedId = true)
    private int rowId;
    @DatabaseField(columnName = "rfc_emisor")
    private String rfcEmisor;
    @DatabaseField(columnName = "rfc_receptor")
    private String rfcReceptor;
    @DatabaseField(columnName = "id")
    private String id;
    @DatabaseField(columnName = "total")
    private String total;
    @DatabaseField(columnName = "qr")
    private String qr;

    public CFDI() {
        super();
    }

    public CFDI(String rfcEmisor, String rfcReceptor, String id, String total, String qr) {
        this.rfcEmisor = rfcEmisor;
        this.rfcReceptor = rfcReceptor;
        this.id = id;
        this.total = total;
        this.qr = qr;
    }

    public String getRfcEmisor() {
        return rfcEmisor;
    }

    public void setRfcEmisor(String rfcEmisor) {
        this.rfcEmisor = rfcEmisor;
    }

    public String getRfcReceptor() {
        return rfcReceptor;
    }

    public void setRfcReceptor(String rfcReceptor) {
        this.rfcReceptor = rfcReceptor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getQr() {
        return qr;
    }

    public void setQr(String qr) {
        this.qr = qr;
    }
}
