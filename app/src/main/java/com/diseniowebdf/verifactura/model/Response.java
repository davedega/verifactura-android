package com.diseniowebdf.verifactura.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by davedega on 14/02/17.
 */

public class Response {
    @SerializedName("msg")
    @Expose
    public String msg;

}
