package com.diseniowebdf.verifactura.view.interfaces;

import com.diseniowebdf.verifactura.model.CFDI;
import com.diseniowebdf.verifactura.presenter.io.VerificaCDFIResponse;

import java.util.List;

/**
 * Created by davedega on 07/02/17.
 */

public interface IVerifacturaInterface {

    void onCFDIResponse(VerificaCDFIResponse response, CFDI cfdi);

    void showCFDIs(List<CFDI> result);

    void onError(String error);

    void hideEmptyMessage();

    void showEmptyMessage();

    void showLoading();

    void hideLoading();
}
