package com.diseniowebdf.verifactura.view.interfaces.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.diseniowebdf.verifactura.R;
import com.diseniowebdf.verifactura.model.CFDI;

import java.util.ArrayList;

/**
 * Created by davedega on 09/02/17.
 */

public class CFDIAdapter extends RecyclerView.Adapter<CFDIAdapter.CFDIHolder> {

    private Context context;
    private ArrayList<CFDI> cfdis;

    public CFDIAdapter(Context context, ArrayList<CFDI> cfdi) {
        this.context = context;
        this.cfdis = cfdi;
    }

    @Override
    public CFDIHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(context)
                .inflate(R.layout.item_cfdis, parent, false);
        return new CFDIHolder(root);
    }

    @Override
    public void onBindViewHolder(CFDIHolder holder, int position) {
        CFDI cfdi = this.cfdis.get(position);
        holder.setEmisor(cfdi.getRfcEmisor());
        holder.setReceptor(cfdi.getRfcReceptor());
        String total = cfdi.getTotal().replaceFirst("^0+(?!$)", "");
        total = String.format("%.2f", Double.valueOf(total));
        holder.setTotal("$ " + total);
        holder.setId(cfdi.getId());
    }

    @Override
    public int getItemCount() {
        return this.cfdis.size();
    }

    public void addCfdi(CFDI repo) {
        this.cfdis.add(0, repo);
        notifyItemInserted(0);
    }

    class CFDIHolder extends RecyclerView.ViewHolder {
        TextView emisor, receptor, total, id;

        public CFDIHolder(View itemView) {
            super(itemView);
            emisor = (TextView) itemView.findViewById(R.id.emisor_textview);
            receptor = (TextView) itemView.findViewById(R.id.receptor_textview);
            total = (TextView) itemView.findViewById(R.id.total_textview);
            id = (TextView) itemView.findViewById(R.id.id_textview);
        }

        public void setEmisor(String emisor) {
            this.emisor.setText(emisor);
        }

        public void setReceptor(String receptor) {
            this.receptor.setText(receptor);
        }

        public void setTotal(String total) {
            this.total.setText(total);
        }

        public void setId(String id) {
            this.id.setText(id);
        }
    }
}
