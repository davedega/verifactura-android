package com.diseniowebdf.verifactura;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.design.widget.FloatingActionButton;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.diseniowebdf.verifactura.model.CFDI;
import com.diseniowebdf.verifactura.presenter.IVeriPresenterInteractor;
import com.diseniowebdf.verifactura.presenter.VerifacturaPresenterLayer;
import com.diseniowebdf.verifactura.presenter.io.VerificaCDFIResponse;
import com.diseniowebdf.verifactura.scan.SimpleScannerActivity;
import com.diseniowebdf.verifactura.view.interfaces.IVerifacturaInterface;
import com.diseniowebdf.verifactura.view.interfaces.adapter.CFDIAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by davedega on 06/02/17.
 */

public class VeriFacturaActivity extends AppCompatActivity implements IVerifacturaInterface {
    private static final int ZXING_CAMERA_PERMISSION = 1;
    private IVeriPresenterInteractor presenter;

    private FloatingActionButton fab;
    private LinearLayout cfdisLayout;
    private LinearLayout emptyLayout;

    private RecyclerView cfdisList;
    private CFDIAdapter adapter;
    private LinearLayoutManager mLayoutManager;
    private ProgressBar progressBar;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verifactura_activity);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.findViewById(R.id.dfwf_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://www.diseno-web-df.com/"));
                startActivity(intent);
            }
        });
        presenter = new VerifacturaPresenterLayer(this, getApplicationContext());
        cfdisLayout = (LinearLayout) findViewById(R.id.cfdis_layout);
        emptyLayout = (LinearLayout) findViewById(R.id.sin_cfdi_layout);
        cfdisList = (RecyclerView) findViewById(R.id.recycler_view);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable()
                .setColorFilter(getResources()
                        .getColor(R.color.primary), PorterDuff.Mode.MULTIPLY);

        mLayoutManager = new LinearLayoutManager(this);
        cfdisList.setLayoutManager(mLayoutManager);

        fab = (FloatingActionButton) findViewById(R.id.scan_qr);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchActivity(SimpleScannerActivity.class);
            }
        });
        presenter.loadCFDIs();
    }

    @Override
    protected void onStart() {
        super.onStart();
        presenter.loadCFDIs();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable("KeyForLayoutManagerState", mLayoutManager.onSaveInstanceState());

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Parcelable state = savedInstanceState.getParcelable("KeyForLayoutManagerState");
        mLayoutManager.onRestoreInstanceState(state);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
                presenter.validateCFDI(result);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    public void launchActivity(Class<?> clss) {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.CAMERA}, ZXING_CAMERA_PERMISSION);
        } else {
            Intent intent = new Intent(this, clss);
            startActivityForResult(intent, 1);
        }
    }

    @Override
    public void onCFDIResponse(final VerificaCDFIResponse response, final CFDI cfdi) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideEmptyMessage();
                if (presenter.saveCFDI(cfdi) == 1) {
                    presenter.sendCFDI(cfdi);
                    adapter.addCfdi(cfdi);
                    cfdisList.smoothScrollToPosition(0);
                }
                Snackbar.make(fab, response.getResult().getCodigoEstatus() + "-" + response.getResult().getEstado(), Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void showCFDIs(List<CFDI> result) {
        adapter = new CFDIAdapter(getApplicationContext(), (ArrayList<CFDI>) result);
        cfdisList.setAdapter(adapter);
        hideEmptyMessage();
        if (result.size() == 0) {
            showEmptyMessage();
        }
    }

    @Override
    public void onError(String error) {
        Log.e("Activity", "onError: " + error);
        Snackbar.make(fab, error, Snackbar.LENGTH_LONG)
                .setAction("Action", null).show();
    }

    @Override
    public void hideEmptyMessage() {
        emptyLayout.setVisibility(View.GONE);
        cfdisLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showEmptyMessage() {
        cfdisLayout.setVisibility(View.GONE);
        emptyLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void hideLoading() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                progressBar.setVisibility(View.GONE);
            }
        });
    }
}
