package com.diseniowebdf.verifactura.presenter.io;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Root;

/**
 * Created by davedega on 07/02/17.
 */

@Root(name = "Consulta")
@NamespaceList({@Namespace(reference = "http://tempuri.org/")})
public class Consulta {
    @Element(name = "expresionImpresa")
    String expresionImpresa;

    public String getExpresionImpresa() {
        return expresionImpresa;
    }

    public void setExpresionImpresa(String expresionImpresa) {
        this.expresionImpresa = expresionImpresa;
    }
}
