package com.diseniowebdf.verifactura.presenter.io;

import com.sncr.xmlutils.SimpleXmlUtils;

import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.mime.TypedInput;

/**
 * Created by davedega on 07/02/17.
 */

public class SoapApi {
    private static final String BASE_URL = "https://consultaqr.facturaelectronica.sat.gob.mx/";

    public SoapApi() {

    }

    public SoapService getSoapService() {
        final Serializer serializer = new Persister();
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setConverter(new SimpleXmlConverter(serializer))
                .build();
        return adapter.create(SoapService.class);
    }

    public VerificaCDFIResponse requestVerificationQr(String qr) {

        Consulta consulta = new Consulta();
        consulta.setExpresionImpresa(qr);

        String s = SimpleXmlUtils.persist(consulta);
        s = s.replaceAll("<\\?xml version=\"1.0\" encoding=\"UTF-8\"\\?>", "");
        TypedInput typedInput = Util.buildBody(s);
        VerificaCDFIResponse response = getSoapService().requestVerificationQr(typedInput);
        return response;
    }

    public interface SoapService {
        @POST("/ConsultaCFDIService.svc")
        @Headers({"Content-Type: text/xml; charset=utf-8",
                "SOAPAction: \"http://tempuri.org/IConsultaCFDIService/Consulta\""})
        VerificaCDFIResponse requestVerificationQr(@Body TypedInput typedInput);
    }

}
