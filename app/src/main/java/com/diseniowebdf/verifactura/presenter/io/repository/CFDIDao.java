package com.diseniowebdf.verifactura.presenter.io.repository;

import android.content.Context;
import android.util.Log;

import com.diseniowebdf.verifactura.model.CFDI;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.List;

/**
 * Created by davedega on 13/02/17.
 */

public class CFDIDao {
    private Dao<CFDI, Integer> cfdiDao = null;
    private DBHelper dbHelper = null;

    public CFDIDao(Context ctx) {
        dbHelper = dbHelper.getInstance(ctx);
    }

    public List<CFDI> getCFDIs() {
        try {
            cfdiDao = dbHelper.getDao(CFDI.class);
//            List<CFDI> cfdis = cfdiDao.queryForAll();
            List<CFDI> cfdis = cfdiDao.queryBuilder().orderBy("_id",false).query();
            return cfdis;
        } catch (SQLException e) {
            Log.e("CFDIDao", "Error getContacts()");
            return null;
        }
    }

    public int insertCFDI(CFDI cfdi) {
        try {
            cfdiDao = dbHelper.getDao(CFDI.class);
            return cfdiDao.create(cfdi);
        } catch (SQLException e) {
            e.printStackTrace();
            return -1;
        }
    }

    public void deleteAll() {
        try {
            cfdiDao = dbHelper.getDao(CFDI.class);
            cfdiDao.executeRawNoArgs("delete from cfdi;");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
