package com.diseniowebdf.verifactura.presenter.io;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * Created by davedega on 07/02/17.
 */
@Root(name = "s:Envelope")
@NamespaceList({@Namespace(reference = "http://schemas.xmlsoap.org/soap/envelope", prefix = "s")})
public class VerificaCDFIResponse {

    @Element(name = "ConsultaResult", required = true)
    @Path("s:Body/ConsultaResponse")
    ConsultaResult result;

    public ConsultaResult getResult() {
        return result;
    }

    public void setResult(ConsultaResult result) {
        this.result = result;
    }
}