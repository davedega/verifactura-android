package com.diseniowebdf.verifactura.presenter.io.repository;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.diseniowebdf.verifactura.model.CFDI;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

/**
 * Created by davedega on 09/02/17.
 */

public class DBHelper extends OrmLiteSqliteOpenHelper {

    private static DBHelper _helperInstance;
    static Context mContext;

    public static final String DATABASE_NAME = "verifacturas";
    public static final int DATABASE_VERSION = 1;

    public static DBHelper getInstance(Context context) {
        if (_helperInstance == null) {
            mContext = context;
            _helperInstance = new DBHelper(context);

        }
        return _helperInstance;
    }

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null,
                DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, CFDI.class);
        } catch (Exception e){
            Log.e("DBHelper", "Exception()", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {

    }

    @Override
    public void close() {
        super.close();
        _helperInstance = null;
    }


}
