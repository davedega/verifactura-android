package com.diseniowebdf.verifactura.presenter;

import android.content.Context;
import android.util.Log;

import com.diseniowebdf.verifactura.R;
import com.diseniowebdf.verifactura.model.CFDI;
import com.diseniowebdf.verifactura.model.Constants;
import com.diseniowebdf.verifactura.model.Response;
import com.diseniowebdf.verifactura.presenter.io.IVerifacturaService;
import com.diseniowebdf.verifactura.presenter.io.SoapApi;
import com.diseniowebdf.verifactura.presenter.io.VerificaCDFIResponse;
import com.diseniowebdf.verifactura.presenter.io.repository.CFDIDao;
import com.diseniowebdf.verifactura.view.interfaces.IVerifacturaInterface;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * Created by davedega on 07/02/17.
 */

public class VerifacturaPresenterLayer implements IVeriPresenterInteractor {

    IVerifacturaInterface view;
    Context ctx;
    CFDIDao cfdiDao;
    List<CFDI> cfdis;
    Retrofit retrofit;
    IVerifacturaService verifacturaService;


    public VerifacturaPresenterLayer(IVerifacturaInterface view, Context ctx) {
        this.view = view;
        this.ctx = ctx;
        cfdiDao = new CFDIDao(this.ctx);

        Gson gson = new GsonBuilder()
                .setLenient()
                .create();


        retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
    }

    @Override
    public int saveCFDI(CFDI cfdi) {
        return cfdiDao.insertCFDI(cfdi);
    }

    @Override
    public void loadCFDIs() {
        cfdis = cfdiDao.getCFDIs();
        view.showCFDIs(cfdis);
    }

    @Override
    public void validateCFDI(String qr) {
        view.showLoading();
        if (!qr.contains("re=") &&
                !qr.contains("&rr=") &&
                !qr.contains("&tt=") &&
                !qr.contains("&id=")) {
            view.hideLoading();
            view.onError(ctx.getString(R.string.no_es_valido));
        } else {
            final String sent = qr;
            qr = qr.replaceAll("\\?re=", " ");
            qr = qr.replaceAll("&rr=", " ");
            qr = qr.replaceAll("&tt=", " ");
            qr = qr.replaceAll("&id=", " ");
            final String[] deco = qr.split(" ");

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final VerificaCDFIResponse response = new SoapApi().requestVerificationQr(sent);
                        Log.i("VerifacturaPresentLay", "validateCFDI() fin");


                        if (response.getResult().getEstado().equals("Vigente")) {
                            CFDI cfdi = new CFDI(deco[1], deco[2], deco[4], deco[3], sent);
                            view.hideLoading();
                            view.onCFDIResponse(response, cfdi);
                        } else if (response.getResult().getEstado().equals("No Encontrado")) {
                            view.hideLoading();
                            view.onError(ctx.getString(R.string.no_encontrado));
                        }
                    } catch (Exception e) {
                        Log.e("PresenterLayer", "validateCFDI(qr) Exception()> " + e);
                        if (e.getCause() instanceof UnknownHostException) {
                            view.hideLoading();
                            view.onError(ctx.getString(R.string.no_tienes_conexion));
                        }

                    }
                }
            }).start();
        }
    }

    @Override
    public void sendCFDI(CFDI cfdi) {
        verifacturaService = retrofit.create(IVerifacturaService.class);
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        Observable<Response> verifacturaObservable = verifacturaService.sendCFDI(cfdi.getRfcEmisor(),
                cfdi.getRfcReceptor(), cfdi.getTotal(), cfdi.getId(), cfdi.getQr(), timestamp.toString());

        verifacturaObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response>() {
                    @Override
                    public void onCompleted() {
                        Log.i("Rx", "onCompleted()");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e("Rx", "onError() " + e.getMessage());
                    }

                    @Override
                    public void onNext(Response response) {
                        Log.i("Rx", "onNext() " + response.msg);
                    }
                });

    }

    @Override
    public void resetDb() {
        cfdiDao.deleteAll();
    }
}