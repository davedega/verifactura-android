package com.diseniowebdf.verifactura.presenter;

import com.diseniowebdf.verifactura.model.CFDI;

/**
 * Created by davedega on 07/02/17.
 */
public interface IVeriPresenterInteractor {

    int saveCFDI(CFDI cfdi);

    void loadCFDIs();

    void validateCFDI(String qr);

    void sendCFDI(CFDI cfdi);

    void resetDb();
}
