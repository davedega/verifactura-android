package com.diseniowebdf.verifactura.presenter.io;

import java.io.UnsupportedEncodingException;

import retrofit.mime.TypedByteArray;
import retrofit.mime.TypedInput;

/**
 * Created by Dave on 2/2/17.
 */
public class Util {


    public static String formatSoapBodyAsStr(String s, String emisor_rfc, String receptor_rfc, String total, String uuid) {
        return ("<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<soap:Envelope " +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " +
                "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                "<soap:Header/>" +
                "<soap:Body>" +
//                "?re={emisor_rfc}&amp;rr={receptor_rfc}&amp;tt={total}&amp;id={uuid}" +
                "?re=" + emisor_rfc + "&amp;rr=" + receptor_rfc + "&amp;tt=" + total + "&amp;id=" + uuid + "" +
                "</soap:Body>" +
                "</soap:Envelope>");
//                .replaceAll("SOAPBODY", s.replaceFirst("<\\?xml version=\"1.0\" encoding=\"UTF-8\"\\?>", "")
//                        .replaceFirst("<\\?xml version=\"1.0\" encoding=\"utf-8\"\\?>", ""));
    }

    public static String formatSoapBodyAsStr(String s) {


        return ("<?xml version=\"1.0\" encoding=\"utf-8\"?>" +
                "<soap:Envelope " +
                "xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" " +
                "xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\" " +
                "xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" +
                "<soap:Header/>" +
                "<soap:Body>" +
//                "?re={emisor_rfc}&amp;rr={receptor_rfc}&amp;tt={total}&amp;id={uuid}" +
                s +
                "</soap:Body>" +
                "</soap:Envelope>");
//                .replaceAll( s.replaceFirst("<\\?xml version=\"1.0\" encoding=\"UTF-8\"\\?>", "")
//                        .replaceFirst("<\\?xml version=\"1.0\" encoding=\"utf-8\"\\?>", ""));
    }

    public static TypedInput formatTypedBody(final String body) {
        try {
            TypedInput in = new TypedByteArray("application/xml", body.getBytes("utf-8"));
            return in;
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static TypedInput buildBody(String s) {
//    public static String buildBody(String s) {
        return formatTypedBody(formatSoapBodyAsStr(s));
//        return formatSoapBodyAsStr(s);
    }


}
