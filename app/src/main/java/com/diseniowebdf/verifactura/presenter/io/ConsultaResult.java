package com.diseniowebdf.verifactura.presenter.io;

import org.simpleframework.xml.Element;
import org.simpleframework.xml.Namespace;
import org.simpleframework.xml.NamespaceList;
import org.simpleframework.xml.Path;
import org.simpleframework.xml.Root;

/**
 * Created by davedega on 07/02/17.
 */
@Root(name = "ConsultaResult")
@NamespaceList({@Namespace(reference = "http://schemas.datacontract.org/2004/07/Sat.Cfdi.Negocio.ConsultaCfdi.Servicio", prefix = "a"),
        @Namespace(reference = "http://www.w3.org/2001/XMLSchema-instance", prefix = "i")})
public class ConsultaResult {

    @Element(name = "Estado")
    String estado;

    @Element(name = "CodigoEstatus")
    String codigoEstatus;


    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getCodigoEstatus() {
        return codigoEstatus;
    }

    public void setCodigoEstatus(String codigoEstatus) {
        this.codigoEstatus = codigoEstatus;
    }
}