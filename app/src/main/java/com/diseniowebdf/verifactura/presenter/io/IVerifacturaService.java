package com.diseniowebdf.verifactura.presenter.io;

import com.diseniowebdf.verifactura.model.CFDI;
import com.diseniowebdf.verifactura.model.Response;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by davedega on 14/02/17.
 */

public interface IVerifacturaService {

    @FormUrlEncoded
    @POST("cfdi.php")
    Observable<Response> sendCFDI(@Field("emisor") String emisor,
                                  @Field("receptor") String receptor,
                                  @Field("total") String total,
                                  @Field("id_factura") String id,
                                  @Field("qr") String qr,
                                  @Field("timestamp") String timestamp);

}
