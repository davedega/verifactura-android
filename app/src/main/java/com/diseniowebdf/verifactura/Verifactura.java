package com.diseniowebdf.verifactura;

import android.app.Application;
import android.util.Log;

import com.newrelic.agent.android.NewRelic;

/**
 * Created by davedega on 14/02/17.
 */

public class Verifactura extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d("Application", "onCreate()");
        NewRelic.withApplicationToken(
                "AA8de633365e5b5a34ddb4530302eea1df2813d100"
        ).start(getApplicationContext());
    }
}
